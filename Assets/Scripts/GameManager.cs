﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[DisallowMultipleComponent]
public class GameManager : MonoBehaviour
{
    // this is singleton
    [HideInInspector]
    public static GameManager instance;
    
    private STATE state;
    public STATE debugState;

    // gamestates
    public enum STATE {RESET, DRAW, WALK, WIN, LOSE, PAUSE, RESTART };
    public delegate void stateChanged(STATE newState);
    public stateChanged OnStateChanged;

    public STATE State
    {
        get => state;
        private set
        {
            state = value;
            debugState = state;
            OnStateChanged?.Invoke(state);
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        State = STATE.PAUSE;
    }

    private void Update()
    {
        switch (State)
        {
            case STATE.RESET:
                State = STATE.DRAW;
                break;
            case STATE.DRAW:
                break;
            case STATE.WALK:
                break;
            case STATE.WIN:
                break;
            case STATE.LOSE:
                break;
            case STATE.PAUSE:
                break;
            case STATE.RESTART:
                State = STATE.RESET;
                break;
        }
    }

    public void CheckIfWon()
    {
        bool won = true;
        foreach(Home house in FindObjectsOfType<Home>())
        {
            if (!house.IsFull)
            {
                won = false;
            }
        }
        if (won)
        {
            State = STATE.WIN;
        }
    }

    public void NextLevel()
    {
        int nextIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (SceneManager.sceneCountInBuildSettings == nextIndex)
        {
            nextIndex = 1;
        }
        SceneManager.LoadScene(nextIndex);
        
    }

    public void CheckIfLost()
    {
        PathFollower[] followers = FindObjectsOfType<PathFollower>();
        int returningCount = 0;
        foreach(PathFollower f in followers)
        {
            if (f.returnBonfirePosition)
            {
                returningCount++;
            }
        }
        if (FindObjectsOfType<PathFollower>().Length <= returningCount + 1) // this is weird
        {
            bool won = true;
            foreach (Home house in FindObjectsOfType<Home>())
            {
                if (!house.IsFull)
                {
                    won = false;
                }
            }
            if (!won)
            {
                State = STATE.LOSE;
            }
        }
        // else - keep going
    }

    public void ChangeGameState(STATE newState)
    {
        State = newState;
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
