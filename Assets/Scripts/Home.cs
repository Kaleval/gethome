﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Home : MonoBehaviour
{
    int placesOcuppied;
    public int maxCount;
    public bool IsFull { get => placesOcuppied == maxCount; }
    public TextMeshPro txtCount;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Person")
        {
            if (placesOcuppied < maxCount)
            {
                placesOcuppied++;
                RefreshText();
                if (placesOcuppied == maxCount)
                {
                    // this house is full
                    GameManager.instance.CheckIfWon();
                }
                Destroy(other.gameObject);
                GameManager.instance.CheckIfLost();
            }
        }
    }

    private void Start()
    {
        GameManager.instance.OnStateChanged += GameStateChanged;
        RefreshText();
    }

    public void GameStateChanged(GameManager.STATE state)
    {
        switch (state)
        {
            case GameManager.STATE.RESET:
                placesOcuppied = 0;
                RefreshText();
                break;
            case GameManager.STATE.DRAW:
                break;
            case GameManager.STATE.WALK:
                break;
            case GameManager.STATE.WIN:
                break;
            case GameManager.STATE.LOSE:
                break;
        }
    }

    void RefreshText()
    {
        txtCount.text = placesOcuppied.ToString() + "/" + maxCount.ToString();
    }


}
