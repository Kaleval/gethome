﻿using UnityEngine;

public class PathFollower : MonoBehaviour
{
    public Animator animator;
    public float speed = 3;
    public int currentWaypoint = 0;
    public int lastWaypoint = 0;

    Vector3 bonfirePos;


    public bool returnBonfire = false;
    public bool returnBonfirePosition = false;
    bool isWalking = false;
    public bool dispatched = false;
    // Start is called before the first frame update
    private void Awake()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
    }

    void Start()
    {
        GameManager.instance.OnStateChanged += GameStateChanged;
    }

    private void OnDestroy()
    {
        GameManager.instance.OnStateChanged -= GameStateChanged;

    }

    // Update is called once per frame
    void Update()
    {
        switch (GameManager.instance.State)
        {
            case GameManager.STATE.DRAW:
                break;
            case GameManager.STATE.WALK:
                if (dispatched)
                {
                    if (returnBonfire)
                    {
                        ReturnToBonfire();
                    }
                    else
                    {
                        Walk();
                    }
                }
                break;

        }
    }

    void UpdateAnim(bool walk)
    {
        if (isWalking != walk)
        {
            animator.SetBool("Walk", walk);
            isWalking = walk;
        }
    }

    void ReturnToBonfire()
    {
        Vector3 waypoint;
        if (returnBonfirePosition)
        {
            waypoint = bonfirePos;
        }
        else
        {
            waypoint = PathManager.instance.Waypoints[currentWaypoint];
        }

        Vector3 translation = (waypoint - transform.position).normalized * Time.deltaTime * speed;
        if (Vector3.Distance(waypoint, transform.position) < translation.magnitude)
        {
            if (returnBonfirePosition)
            {
                UpdateAnim(false);
                transform.LookAt(PathManager.instance.Waypoints[0]);
                GameManager.instance.CheckIfLost();
            }
            else
            {
                // waypoint reached
                if (currentWaypoint == 0)
                {
                    returnBonfirePosition = true;
                    // should change - last point reached
                    //GameManager.instance.ChangeGameState(GameManager.STATE.DRAW);
                }
                else
                {
                    currentWaypoint--;
                }
            }
        }
        else
        {
            UpdateAnim(true);
            transform.LookAt(transform.position + translation);
            transform.position += translation;
        }
    }

    void Walk()
    {
        Vector3 waypoint = PathManager.instance.Waypoints[currentWaypoint];
        Vector3 translation = (waypoint - transform.position).normalized * Time.deltaTime * speed;
        if (Vector3.Distance(waypoint, transform.position) < translation.magnitude)
        {
            // waypoint reached
            if (currentWaypoint == lastWaypoint)
            {
                // should change - last point reached
                //GameManager.instance.ChangeGameState(GameManager.STATE.DRAW);
            }
            else
            {
                currentWaypoint++;
            }
        }
        else
        {
            Ray ray = new Ray(transform.position, transform.forward);
            if (Physics.Raycast(ray, out RaycastHit hit, 1f, LayerMask.GetMask("Obstacle")))
            {
                //if (hit.collider.gameObject.tag == "Obstacle")
                //{
                    returnBonfire = true;
                //}
                UpdateAnim(false);

            }
            else
            {
                UpdateAnim(true);
                transform.LookAt(transform.position + translation);
                transform.position += translation;
            }
        }
    }

    public void GameStateChanged(GameManager.STATE state)
    {
        switch (state)
        {
            case GameManager.STATE.DRAW:
                bonfirePos = transform.position;
                break;
            case GameManager.STATE.WALK:
                lastWaypoint = PathManager.instance.Waypoints.Count - 1;
                currentWaypoint = 0;
                break;
            case GameManager.STATE.RESET:
                break;
            case GameManager.STATE.WIN:
                break;
            case GameManager.STATE.LOSE:
                break;
            case GameManager.STATE.PAUSE:
                break;
            case GameManager.STATE.RESTART:
                Destroy(gameObject);
                break;
        }
    }


}
