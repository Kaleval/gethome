﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class PathManager : MonoBehaviour
{
    List<Vector3> waypoints;

    Vector3 debugPosition;

    [HideInInspector]
    public static PathManager instance;

    public List<Vector3> Waypoints { get => waypoints; }


    Vector2 lastUV;

    // init singleton
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        waypoints = new List<Vector3>();
        GameManager.instance.OnStateChanged += GameStateChanged;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (GameManager.instance.State)
        {
            case GameManager.STATE.DRAW:
                DrawPath();
                break;
            case GameManager.STATE.WALK:
                break;
            case GameManager.STATE.RESET:
                break;
            case GameManager.STATE.WIN:
                break;
            case GameManager.STATE.LOSE:
                break;
            case GameManager.STATE.PAUSE:
                break;
            case GameManager.STATE.RESTART:
                break;
        }

    }

    public void GameStateChanged(GameManager.STATE state)
    {
        switch (state)
        {
            case GameManager.STATE.RESET:
                ResetWaypoints();
                break;
            case GameManager.STATE.DRAW:
                break;
            case GameManager.STATE.WALK:
                break;
            case GameManager.STATE.WIN:
                break;
            case GameManager.STATE.LOSE:
                break;
            case GameManager.STATE.PAUSE:
                break;
            case GameManager.STATE.RESTART:
                break;
        }
    }

    // draws waypoints every frame
    void DrawPath()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Waypoints.Clear();
        }
        else if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            int mask = LayerMask.GetMask("Default");
            if (Physics.Raycast(ray, out RaycastHit hit, float.PositiveInfinity, mask))
            {
                Vector3 pos = hit.point;
                int cnt = Waypoints.Count;
                FindObjectOfType<VisualDraw>().Draw(hit.point);
                if (cnt == 0)
                {
                    Waypoints.Add(pos);
                    debugPosition = pos;
                }
                else
                {
                    Vector3 prevPos = Waypoints[cnt - 1];
                    if (Vector3.Distance(pos, prevPos) > 0.7f)
                    {
                        Waypoints.Add(pos);
                        debugPosition = pos;
                    }
                }
            }
        }
        else //if (Input.GetMouseButtonUp(0))
        {
            // allow to walk
            if (Waypoints.Count > 0)
            {
                Debug.Log("CheckIfStartHit " + CheckIfStartHit());
                Debug.Log("CheckIfAllMustCrossHit " + CheckIfAllMustCrossHit());
                if (CheckIfStartHit() && CheckIfAllMustCrossHit())
                {
                    GameManager.instance.ChangeGameState(GameManager.STATE.WALK);
                }
                else
                {
                    GameManager.instance.ChangeGameState(GameManager.STATE.RESET);
                }
            }
        }
    }


    void ResetWaypoints()
    {
        FindObjectOfType<VisualDraw>().Clear();
        Waypoints.Clear();
    }

    bool CheckIfStartHit()
    {
        // should be only one start
        GameObject obj = GameObject.FindGameObjectWithTag("Start");
        Collider col = obj.GetComponent<Collider>();

        return col.bounds.Contains(Waypoints[0]);
    }

    bool CheckIfAllMustCrossHit()
    {
        // should be only one start
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("MustCross"))
        {
            Collider col = obj.GetComponent<Collider>();
            bool crossed = false;
            foreach (Vector3 pos in Waypoints)
            {
                crossed |= col.bounds.Contains(pos);
                if (crossed)
                {
                    // has crossed
                    break;
                }
            }
            if (!crossed)
            {
                Debug.Log(obj.name + " not crosed");
                // if one house not crossed
                return false;
            }
        }
        // all houses crossed
        return true;
    }

    // debg draw path
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 1, 1);
        Gizmos.DrawWireSphere(debugPosition, 0.3f);
        if (Waypoints != null)
        {
            int cnt = Waypoints.Count;
            for (int i = 0; i < cnt; i++)
            {
                Vector3 pos = Waypoints[i];
                Gizmos.color = new Color(0, 1, 0);
                Gizmos.DrawWireSphere(pos, 0.3f);
                if (i < cnt - 1)
                {
                    Vector3 nextPos = Waypoints[i + 1];
                    Gizmos.DrawLine(pos, nextPos);
                }
            }
        }
    }
}
