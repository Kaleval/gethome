﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class PeopleManager : MonoBehaviour
{
    [HideInInspector]
    public static PeopleManager instance;
    public List<GameObject> personPrefabs;


    List<Vector3> initialPositions;

    int peopleReturnedHome = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        initialPositions = new List<Vector3>();
        foreach (PathFollower person in FindObjectsOfType<PathFollower>())
        {
            initialPositions.Add(person.transform.position);
        }
        GameManager.instance.OnStateChanged += GameStateChanged;
    }

    public Vector3 GetReturnPosition()
    {
        return initialPositions[peopleReturnedHome++];
    }

    // Update is called once per frame
    void Update()
    {
        switch (GameManager.instance.State)
        {
            case GameManager.STATE.RESET:
                break;
            case GameManager.STATE.DRAW:
                break;
            case GameManager.STATE.WALK:
                if (Input.GetMouseButtonDown(0))
                {
                    foreach(PathFollower f in FindObjectsOfType<PathFollower>())
                    {
                        if (f.dispatched == false)
                        {
                            f.dispatched = true;
                            break;
                        }
                    }
                }
                break;
            case GameManager.STATE.WIN:
                break;
            case GameManager.STATE.LOSE:
                break;
            case GameManager.STATE.PAUSE:
                break;
            case GameManager.STATE.RESTART:
                break;
        }
    }


    public void GameStateChanged(GameManager.STATE state)
    {
        switch (state)
        {
            case GameManager.STATE.RESET:
                break;
            case GameManager.STATE.DRAW:
                break;
            case GameManager.STATE.WALK:
                break;
            case GameManager.STATE.WIN:
                break;
            case GameManager.STATE.LOSE:
                break;
            case GameManager.STATE.PAUSE:
                break;
            case GameManager.STATE.RESTART:
                ResetPositions();
                break;
        }
    }

    void ResetPositions()
    {
        foreach (Vector3 pos in initialPositions)
        {
            int r = Random.Range(0, personPrefabs.Count - 1);
            Instantiate(personPrefabs[r], pos, Quaternion.Euler(0, 1, 0), transform);
        }
    }
}
