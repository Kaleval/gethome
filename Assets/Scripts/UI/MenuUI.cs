﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
    public GameObject loadingTitle;
    public GameObject menu;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            LoadLevel();
        }
    }

    public void LoadLevel()
    {
        menu.SetActive(false);
        loadingTitle.SetActive(true);
        SceneManager.LoadScene(1);
    }
}
