﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    public GameObject canvaPause;
    public GameObject canvaWin;
    public GameObject canvaLose;
    public GameObject canvaDraw;
    public GameObject canvaWalk;
    // Start is called before the first frame update
    private void Awake()
    {

        SetCanva(canvaPause, false);
        SetCanva(canvaLose, false);
        SetCanva(canvaWin, false);
        SetCanva(canvaDraw, false);
        SetCanva(canvaWalk, false);
        GameManager.instance.OnStateChanged += GameStateChanged;
    }
    void Start()
    {
    }

    public void OnMenuBtnPressed()
    {
        GameManager.instance.ReturnToMenu();
    }

    public void OnRestartBtnPressed()
    {
        GameManager.instance.ChangeGameState(GameManager.STATE.RESTART);
    }

    public void OnNextBtnPressed()
    {

        GameManager.instance.NextLevel();
    }

    public void OnResumeBtnPressed()
    {
        GameManager.instance.ChangeGameState(GameManager.STATE.RESET);
    }

    public void OnTipPressed()
    {
        GameManager.instance.ChangeGameState(GameManager.STATE.DRAW);
    }

    public void GameStateChanged(GameManager.STATE state)
    {
        switch (state)
        {
            case GameManager.STATE.RESET:
                SetCanva(canvaPause, false);
                SetCanva(canvaLose, false);
                SetCanva(canvaWin, false);
                SetCanva(canvaDraw, false);
                SetCanva(canvaWalk, false);
                break;
            case GameManager.STATE.DRAW:
                SetCanva(canvaPause, false);
                SetCanva(canvaLose, false);
                SetCanva(canvaWin, false);
                SetCanva(canvaDraw, true);
                SetCanva(canvaWalk, false);
                break;
            case GameManager.STATE.WALK:
                SetCanva(canvaPause, false);
                SetCanva(canvaLose, false);
                SetCanva(canvaWin, false);
                SetCanva(canvaDraw, false);
                SetCanva(canvaWalk, true);
                break;
            case GameManager.STATE.WIN:
                SetCanva(canvaPause, false);
                SetCanva(canvaLose, false);
                SetCanva(canvaWin, true);
                SetCanva(canvaDraw, false);
                SetCanva(canvaWalk, false);
                break;
            case GameManager.STATE.LOSE:
                SetCanva(canvaPause, false);
                SetCanva(canvaLose, true);
                SetCanva(canvaWin, false);
                SetCanva(canvaDraw, false);
                SetCanva(canvaWalk, false);
                break;
            case GameManager.STATE.PAUSE:
                SetCanva(canvaPause, true);
                SetCanva(canvaLose, false);
                SetCanva(canvaWin, false);
                SetCanva(canvaDraw, false);
                SetCanva(canvaWalk, false);
                break;
            case GameManager.STATE.RESTART:
                break;
        }
    }

    void SetCanva(GameObject canva, bool state)
    {
       // canva.interactable = state;
      //  canva.blocksRaycasts = state;
        canva.gameObject.SetActive(state);
        //canva.alpha = state ? 1f : 0f;
    }
}
