﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualDraw : MonoBehaviour
{
    public LineRenderer line;
    // Start is called before the first frame update
    void Start()
    {
        Clear();
    }

    public void Draw(Vector3 position)
    {
        int oldCount = line.positionCount;
        Vector3[] oldPos = new Vector3[oldCount];
        line.GetPositions(oldPos);
        Vector3[] newPos = new Vector3[oldCount + 1];
        for (int i = 0; i < oldCount; i++)
        {
            newPos[i] = oldPos[i];
        }
        newPos[oldCount] = position + Vector3.up * 0.3f;
        line.positionCount = oldCount + 1;
        line.SetPositions(newPos);


    }


    public void Clear()
    {
        Vector3[] pos = new Vector3[0];
        line.SetPositions(pos);
        line.positionCount = 0;
    }
}
