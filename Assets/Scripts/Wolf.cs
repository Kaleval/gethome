﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Person")
        {
            Destroy(other.gameObject);
            GameManager.instance.CheckIfLost();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
